## Overview
We need to increase the coverage of the quality tagging among academic images in our existing system. The quality tagging of the images can be used by other teams to generate good quality content in the learning domain.We can increase the quality of images in our system by tagging it against various cluster labels.

## Objective
We have to increase the quality of images in our system by tagging it against various field labels.And for that purpose We are building a multiclass model for field tagging divided based on each discipline group (Life Sciences, Physical Sciences, Mathematics, Ecology, Social Sciences).


## Summary & Conclusion

### 3. Ecology Discipline group based field-

### Experiment 1

 We have divided field based on Ecology Discipline group and used SMOTE oversampling technique to handle class imbalance problem.

Data Distribution-

      Training data-  3738 Test data- 660

Model Results-

     Test accuracy- 0.63182%

     AUC score- 0.71%

     precision, recall & F1 score- please refer to this link- (https://drive.google.com/file/d/1ci-PLRuYAhaY--9zx2dVlWD2xVfFom7I/view?usp=sharing) 

 

### 4. Physical Science Discipline group based field-

### Experiment 1

 We have divided field based on Physical Science Discipline group and used SMOTE oversampling technique to handle class imbalance problem.

Data Distribution-

      Training data-  10108  Test data- 1784

Model Results-

     Test accuracy- 0.45123%

     AUC score- 0.69%

     precision, recall & F1 score- please refer to this link- (https://drive.google.com/file/d/13zktJ2w4MVJKrP4ztvR2fCDDUFRjp-IV/view?usp=sharing)

### 5. Life Science Discipline group based field- 

### Experiment 1

 We have divided field based on Life Science Discipline group and used SMOTE oversampling technique to handle class imbalance problem.

Data Distribution-

      Training data-  3962  Test data- 700

Model Results-

     Test accuracy- 0.43286%

     AUC score- 0.66%

     precision, recall & F1 score- please refer to this link- (https://drive.google.com/file/d/1enpS4jcaKyYZNjE8eRdxosuaKJvEA-Yu/view?usp=sharing)

